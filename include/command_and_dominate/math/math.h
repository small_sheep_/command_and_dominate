#ifndef COMMAND_AND_DOMINATE_SRC_MATH_MATH_H_
#define COMMAND_AND_DOMINATE_SRC_MATH_MATH_H_

#include "command_and_dominate/math/vector/vector.h"
#include "command_and_dominate/math/matrix/matrix.h"

#include "command_and_dominate/math/type_ptr.h"

#endif // !COMMAND_AND_DOMINATE_SRC_MATH_MATH_H_
