#ifndef COMMAND_AND_DOMINATE_MATH_TYPE_PTR_H_
#define COMMAND_AND_DOMINATE_MATH_TYPE_PTR_H_

#include "command_and_dominate/math/vector/vector.h"
#include "command_and_dominate/math/matrix/matrix.h"

template<typename T> T const *GetValuePointer(Vector2D<T> const &data);
template<typename T> T const *GetValuePointer(Vector3D<T> const &data);
template<typename T> T const *GetValuePointer(Vector4D<T> const &data);
template<typename T> T const *GetValuePointer(Matrix4x4<T> const &data);

#include "command_and_dominate/math/type_ptr.hpp"

#endif  // COMMAND_AND_DOMINATE_MATH_TYPE_PTR_H_
