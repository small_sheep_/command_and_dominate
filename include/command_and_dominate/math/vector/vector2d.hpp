#ifndef COMMAND_AND_DOMINATE_SRC_MATH_VECTOR_VECTOR_2D_HPP_
#define COMMAND_AND_DOMINATE_SRC_MATH_VECTOR_VECTOR_2D_HPP_

template<typename value_type>
Vector2D<value_type>::Vector2D() : x(0), y(0) {}

template<typename value_type>
Vector2D<value_type>::Vector2D(value_type const scalar)
    : x(scalar), y(scalar) {}

template<typename value_type>
Vector2D<value_type>::Vector2D(value_type _x, value_type _y) : x(_x), y(_y) {}

template<typename value_type>
Vector2D<value_type>::~Vector2D() {}

#endif // !COMMAND_AND_DOMINATE_SRC_MATH_VECTOR_VECTOR_2D_HPP_
