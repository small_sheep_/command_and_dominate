#ifndef COMMAND_AND_DOMINATE_SRC_UNIFORM_BLOCK_UNIFORM_BLOCK_H_
#define COMMAND_AND_DOMINATE_SRC_UNIFORM_BLOCK_UNIFORM_BLOCK_H_

#include "command_and_dominate/uniform_block/uniform_block_forward.h"
#include <map>
#include "small_utility/string/string.h"
#include "command_and_dominate/glad.h"
#include "command_and_dominate/math/type_ptr.h"

using small_utility::string_stuff::String;

class UniformBlock {
 public:
  static void Terminate();
  static void Make(int const id, unsigned int const size,
                   unsigned int const bind_point);
  static UniformBlock const &FetchById(int const id);
  UniformBlock(int const bind_point);
  ~UniformBlock();

  template<typename T>
  void FillBuffer(int const offset, T const &data) const {
    glBindBuffer(GL_UNIFORM_BUFFER, id_);
    glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(T),
                    GetValuePointer(data));
    //glBindBuffer(GL_UNIFORM_BUFFER, 0);
  }
  void Delete();

  unsigned int Id() const { return id_; }
  unsigned int BindPoint() const { return bind_point_; }

 private:
  static std::map<int, UniformBlock> all_;

  unsigned int id_;
  unsigned int bind_point_;
};

#endif // !COMMAND_AND_DOMINATE_SRC_UNIFORM_BLOCK_UNIFORM_BLOCK_H_
