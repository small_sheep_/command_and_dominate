#ifndef COMMAND_AND_DOMINATE_SRC_IMAGE_IMAGE2D_H_
#define COMMAND_AND_DOMINATE_SRC_IMAGE_IMAGE2D_H_

#include "command_and_dominate/image/image2d_forward.h"
#include <map>

// TODO(small_sheep_ 1178550325@qq.com) please finish this class Image2D.
class Image2D {
 public:
  static void MakeFromFile(int const id, char const *const file_name);
  static Image2D &FetchById(int const id);
  static std::map<int, Image2D> &All() { return Image2D::all_; }
  static void Initialize();
  Image2D(int const width, int const height, int const num_channels,
          unsigned char *const data);
  //Image2D(Image2D const &image2d) = delete;
  ~Image2D();
  void Bind() const;
  unsigned int Id() const { return id_; }

 private:
  void Generate() const;

  static std::map<int, Image2D> all_;
  unsigned int id_;
  int const width_, height_, num_channels_;
  unsigned char *const data_;
};

#endif // !COMMAND_AND_DOMINATE_SRC_IMAGE_IMAGE2D_H_
