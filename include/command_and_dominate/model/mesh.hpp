#ifndef COMMAND_AND_DOMINATE_SRC_MODEL_MESH_HPP_
#define COMMAND_AND_DOMINATE_SRC_MODEL_MESH_HPP_

#include <map>
#include "command_and_dominate/glad.h"
#include "command_and_dominate/model/vertex.h"

template<typename T>
Mesh<T> Mesh<T>::Create() {
  return Mesh<T>();
}

template<typename T>
Mesh<T> Mesh<T>::Create(std::vector<Vertex<T>> const &vertices,
                        std::vector<unsigned int> const &indices,
                        std::vector<int> const &image_ids) {
  Mesh<T> mesh(vertices, indices, image_ids);
  return mesh;
}

template<typename T> Mesh<T>::Mesh() {
  Initialize();
}

template<typename T>
Mesh<T>::Mesh(std::vector<Vertex<T>> const &vertices,
              std::vector<unsigned int> const &indices,
              std::vector<int> const &image_ids)
    : vertices_(vertices), indices_(indices), image_ids_(image_ids) {
  Initialize();
}

template<typename T>
Mesh<T>::~Mesh() {
  Delete();
}

template<typename T>
void Mesh<T>::Render() const {
  // TODO(small_sheep_ 1178550325@qq.com) please remove the argument
  // `ShaderPtr const &shader_ptr' from this function.
  //  But why? Now you can think about that if we can pass the shader to this
  //  function to use, we can also use it BEFORE the function so that
  //  we can improve the performance by call glUseProgram only once rather than
  //  call it repeatedly.
  // Attention, before render something on the screen, you should call
  // `shader.Use()' to draw with correct shader.
  Image2D::FetchById(image_ids_[0]).Bind();
  glBindVertexArray(vao_);
  glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT,
                 static_cast<void *>(0));
  // We do not call glBindVertexArray(0) to unbind the vertex array because
  // all the time we will call glBindVertexArray(vao) before operate on Vertex
  // Array.
  //glBindVertexArray(0);
}

template<typename T>
void Mesh<T>::Delete() {
  glDeleteVertexArrays(1, &vao_);
  glDeleteBuffers(1, &vbo_);
  glDeleteBuffers(1, &ebo_);
}

template<typename T>
void Mesh<T>::Initialize() {
  glGenVertexArrays(1, &vao_);
  glBindVertexArray(vao_);

  glGenBuffers(1, &vbo_);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex<T>) * vertices_.size(),
               &(vertices_[0]), GL_STATIC_DRAW);

  glGenBuffers(1, &ebo_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices_.size(),
               &(indices_[0]), GL_STATIC_DRAW);

  // position
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex<T>), nullptr);
  // normal
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex<T>),
                        reinterpret_cast<void *>(offsetof(Vertex<T>, normal)));
  // texture coordinate
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex<T>),
                        reinterpret_cast<void *>(offsetof(Vertex<T>,
                                                          texture_coordinate)));

  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template<typename T>
std::map<int, Mesh<T>> Mesh<T>::all_;

#endif // !COMMAND_AND_DOMINATE_SRC_MODEL_MESH_HPP_
