#ifndef COMMAND_AND_DOMINATE_SRC_MODEL_MESH_H_
#define COMMAND_AND_DOMINATE_SRC_MODEL_MESH_H_

#include <vector>
#include "command_and_dominate/image/image2d_forward.h"
#include "command_and_dominate/model/mesh_forward.h"
#include "command_and_dominate/shader/shader_forward.h"
#include "command_and_dominate/model/vertex_forward.h"

template<typename T> class Mesh {
 public:
  static Mesh<T> Create();
  static Mesh<T> Create(std::vector<Vertex<T>> const &vertices,
                        std::vector<unsigned int> const &indices,
                        std::vector<int> const &image_ids);
  Mesh();
  Mesh(std::vector<Vertex<T>> const &vertices,
       std::vector<unsigned int> const &indices,
       std::vector<int> const &image_ids_);
  ~Mesh();

  void Render() const;
  void Delete();

 private:
  void Initialize();

  static std::map<int, Mesh<T>> all_;
  unsigned int vao_, vbo_, ebo_;
  std::vector<Vertex<T>> vertices_;
  std::vector<unsigned int> indices_;
  std::vector<int> image_ids_;
};

#include "command_and_dominate/model/mesh.hpp"

#endif // !COMMAND_AND_DOMINATE_SRC_MODEL_MESH_H_
