#ifndef COMMAND_AND_DOMINATE_SHADER_SHADER_H_
#define COMMAND_AND_DOMINATE_SHADER_SHADER_H_

#include "command_and_dominate/shader/shader_forward.h"
#include <map>
#include "command_and_dominate/math/math.h"
#include "command_and_dominate/glad.h"
#include "command_and_dominate/shader/shader_type_forward.h"
#include "command_and_dominate/uniform_block/uniform_block_forward.h"

class Shader {
 public:
  static void Terminate();
  static void MakeFromFile(int const id, char const *const vertex_file_name,
                           char const *const fragment_file_name,
                           char const *const geometry_file_name);
  Shader();
  ~Shader();

  static Shader const &FetchById(int const id);
  void Use() const;
  void Delete() const;
  void BindUniformBlock(char const *const name,
                        int const uniform_block_id) const;
  // ATTENTION, you should call Use() before calling those 'Set*' function.
  //void SetLight(char const *const name, LightPtr const &light_ptr);
  void Set1Bool(char const *const name, bool const data) const;
  void Set1Float(char const *const name, float const data) const;
  void Set1Int(char const *const name, int const data) const;
  template<typename T>
  void SetVector2D(char const *const name, Vector2D<T> const &data) const;
  template<typename T>
  void SetVector3D(char const *const name, Vector3D<T> const &data) const;
  template<typename T>
  void SetMatrix4x4(char const *const name, Matrix4x4<T> const &data) const;

  unsigned int Id() const { return id_; }
  unsigned int GetUniformLocation(char const *const name) const;

 private:
  static void CheckErrors(unsigned int const id, ShaderType const shader_type);

  static std::map<int, Shader> all_;
  static int current_use_shader_;
  unsigned int id_;  // This is glShaderProgram id, not id in Shader::all_[id].
};


#endif  // COMMAND_AND_DOMINATE_SHADER_SHADER_H_
