#include "command_and_dominate/shader/shader.h"
#include <cstdio>
#include <cstring>
#include <stdexcept>
#include "small_utility/log/logger.h"
#include "command_and_dominate/math/math.h"
#include "small_utility/string/string.h"
#include "command_and_dominate/shader/shader_type.h"
#include "command_and_dominate/uniform_block/uniform_block.h"

void Shader::Terminate() {
  all_.clear();
}

void Shader::MakeFromFile(int const id,
                          char const *const vertex_file_name,
                          char const *const fragment_file_name,
                          char const *const geometry_file_name) {
  small_utility::log_stuff::Debug("creating shaders");
  char const *const filenames[3] =
      {vertex_file_name, fragment_file_name, geometry_file_name};
  // Compile shaders from files, and attach them to the shader program.
  FILE *file_ptrs[3];
  unsigned int shader_id;
  int file_size;
  int i = 0;
  using small_utility::string_stuff::String;
  try {
    // Firstly open three files.
    for (; i != 3; ++i) {
      file_ptrs[i] = fopen(filenames[i], "rb");
      if (!file_ptrs[i]) {
        String error_buffer("unable to open file ");
        error_buffer += filenames[i];
        small_utility::log_stuff::Error(error_buffer.ConstData());
        throw std::runtime_error(error_buffer.ConstData());
      }
    }
  } catch (std::exception const &e) {
    // If some errors occur, close all the files opened before.
    for (int j = 0; j != i; ++j) {
      fclose(file_ptrs[j]);
    }
    throw;
  }
  all_.try_emplace(id);
  // Then read the files and compile them into shaders.
  for (int i = 0; i != 3; ++i) {
    fseek(file_ptrs[i], 0, SEEK_END);
    file_size = ftell(file_ptrs[i]);
    fseek(file_ptrs[i], 0, SEEK_SET);
    char *buffer = new char[file_size + 1];
    buffer[file_size] = '\0';
    fread(buffer, file_size, 1, file_ptrs[i]);
    fclose(file_ptrs[i]);
    if (i == 0) {
      shader_id = glCreateShader(GL_VERTEX_SHADER);
    } else if (i == 1) {
      shader_id = glCreateShader(GL_FRAGMENT_SHADER);
    } else if (i == 2) {
      shader_id = glCreateShader(GL_GEOMETRY_SHADER);
    }
    glShaderSource(shader_id, 1, &buffer, 0);
    glCompileShader(shader_id);
    CheckErrors(shader_id, static_cast<ShaderType>(i + 1));
    glAttachShader(Shader::FetchById(id).Id(), shader_id);
    glDeleteShader(shader_id); // This just sets the need_deleting flag.
    delete[] buffer;
  }
  glLinkProgram(Shader::FetchById(id).Id());
  CheckErrors(Shader::FetchById(id).Id(), ShaderType::kShaderTypeProgram);
}

Shader::Shader() : id_(glCreateProgram()) {}

Shader::~Shader() {
  Delete();
}

Shader const &Shader::FetchById(int const id) {
  if (!all_.contains(id)) {
    throw std::runtime_error("Can not find shader.");
  }
  return all_.at(id);
}

void Shader::Use() const {
  glUseProgram(id_);
}

void Shader::BindUniformBlock(char const *const name,
                              int const uniform_block_id) const {
  glUniformBlockBinding(id_, glGetUniformBlockIndex(id_, name),
                        UniformBlock::FetchById(uniform_block_id).BindPoint());
}

void Shader::Delete() const {
  small_utility::log_stuff::Debug("Deleting shader program.");
  glDeleteProgram(id_);
}

//void Shader::SetLight(char const *const name, LightPtr const &light_ptr) {
//  
//}

void Shader::Set1Int(char const *const name, int const data) const {
  glUniform1i(GetUniformLocation(name), data);
}

void Shader::Set1Bool(char const *const name, bool const data) const {
  Set1Int(name, static_cast<int const>(data));
}

void Shader::Set1Float(char const *const name, float const data) const {
  glUniform1f(GetUniformLocation(name), data);
}

template<typename T>
void Shader::SetVector2D(char const *const name,
                         Vector2D<T> const& data) const {
  glUniform2fv(GetUniformLocation(name), 1, GetValuePtr(data));
}

template<typename T>
void Shader::SetVector3D(char const *const name,
                         Vector3D<T> const& data) const {
  glUniform3fv(GetUniformLocation(name), 1, GetValuePtr(data));
}

template<typename T>
void Shader::SetMatrix4x4(char const *const name,
                          Matrix4x4<T> const& data) const {
  glUniformMatrix4fv(GetUniformLocation(name), 1, false, GetValuePtr(data));
}

unsigned int Shader::GetUniformLocation(char const *const name) const {
  return glGetUniformLocation(id_, name);
}

void Shader::CheckErrors(unsigned int const id, ShaderType const shader_type) {
  int successful;
  char const *shader_type_string;
  if (shader_type == ShaderType::kShaderTypeProgram) {
    glGetProgramiv(id, GL_LINK_STATUS, &successful);
    shader_type_string = "program";
  } else {
    glGetShaderiv(id, GL_COMPILE_STATUS, &successful);
    if (shader_type == ShaderType::kShaderTypeVertexShader) {
      shader_type_string = "vertex_shader";
    } else if (shader_type == ShaderType::kShaderTypeFragmentShader){
      shader_type_string = "fragment_shader";
    } else if (shader_type == ShaderType::kShaderTypeGeometryShader){
      shader_type_string = "geometry_shader";
    }
  }
  if (!successful) {
    int const log_size = 128;
    char info_log[log_size] = { 0 };
    if (shader_type == ShaderType::kShaderTypeProgram) {
      glGetProgramInfoLog(id, log_size - 1, nullptr, info_log);
    } else {
      glGetShaderInfoLog(id, log_size - 1, nullptr, info_log);
    }
    using small_utility::string_stuff::String;
    String error_buffer("Some errors occurred in ");
    error_buffer += shader_type_string;
    error_buffer += ", info:\n";
    error_buffer += info_log;
    small_utility::log_stuff::Error(error_buffer);
    throw std::runtime_error(error_buffer.ConstData());
  }
}

std::map<int, Shader> Shader::all_;
int Shader::current_use_shader_;
