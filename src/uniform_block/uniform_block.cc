#include "command_and_dominate/uniform_block/uniform_block.h"
#include "glad/glad.h"

std::map<int, UniformBlock> UniformBlock::all_;

void UniformBlock::Terminate() {
  all_.clear();
}

void UniformBlock::Make(int const id, unsigned int const size,
                        unsigned int const bind_point) {
  all_.try_emplace(id, bind_point);

  glBindBuffer(GL_UNIFORM_BUFFER, UniformBlock::FetchById(id).Id());
  glBufferData(GL_UNIFORM_BUFFER, size, nullptr, GL_STATIC_DRAW);
  glBindBufferBase(GL_UNIFORM_BUFFER, bind_point,
                   UniformBlock::FetchById(id).Id());
  // Or glBindBufferRange(GL_UNIFORM_BUFFER, bind_point_, id_, 0, size_);
}

UniformBlock::UniformBlock(int bind_point) : bind_point_(bind_point) {
  glGenBuffers(1, &id_);
}

UniformBlock::~UniformBlock() {
  Delete();
}

UniformBlock const &UniformBlock::FetchById(int const id) {
  if (!all_.contains(id)) {
    throw std::runtime_error("Can not find uniform block.");
  }
  return all_.at(id);
}

void UniformBlock::Delete() {
  glDeleteBuffers(1, &id_);
}
