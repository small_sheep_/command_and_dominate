#include "command_and_dominate/game/game.h"
#include <cstdio>
#include <exception>
#include "small_utility/log/logger.h"
#include "small_utility/utility/utility.h"
#include "glad/glad.h"
#include "command_and_dominate/game_object/rendered_object.h"
#include "command_and_dominate/image/image2d.h"
#include "command_and_dominate/model/mesh.h"
#include "command_and_dominate/shader/shader.h"
#include "command_and_dominate/uniform_block/uniform_block.h"

bool Game::keys_[348] = { false };
bool Game::keys_pressed_single_times_[348] = { false };
//Camera Game::camera_(glm::vec3(0.0f, 1.0f, 0.0f), 1.0f,
//                    glm::vec3(0.0f), glm::vec3(0.0f),
//                    nullptr, glm::vec3(0.0f), 0.0f, 0.0f, 0.0f,
//                    "test_camera_", "test_camera_ui");

void Game::Initialize(int argc, char *argv[]) {
  try {
    small_utility::log_stuff::Logger::Instance().SetPatternDefault(
        "[$(time)] $(log_level) $(file_name):$(line) in function"
        " `$(function_name)': $(content)\n");
    small_utility::log_stuff::Logger::Instance().SetLogLevelMinimum(
        small_utility::log_stuff::LogLevel::kLogLevelDebug);

    Image2D::Initialize();
    Window::Initialize();
    small_utility::log_stuff::Debug("game initialized");
  } catch (std::exception const &e) {
    throw;
  }
}

void Game::Terminate() {
  Shader::Terminate();
  UniformBlock::Terminate();
  Window::Terminate();
  small_utility::log_stuff::Debug("game terminated");
  small_utility::log_stuff::Logger::Instance().WriteToTarget("console");
}

GamePtr Game::Create(WindowPtr const &window_ptr) {
  GamePtr game_ptr = std::make_shared<Game>(window_ptr);
  Shader::MakeFromFile(0, "shader/test.vs", "shader/test.fs", "shader/test.gs");
  UniformBlock::Make(0, 1 * sizeof(Matrix4x4<float>), 0);
  Shader::FetchById(0).BindUniformBlock("matrices", 0);
  game_ptr->game_state_ = GameState::kGameStateStarted;
  return game_ptr;
}

Game::Game(WindowPtr const &window_ptr)
    : window_ptr_(window_ptr),
      window_clear_color_(0.5f, 0.5f, 0.5f, 1.0f),
      blend_(true), blend_default_(true),
      pause_(false), pause_default_(false),
      game_state_(GameState::kGameStateStarted) {}

Game::~Game() {
  Delete();
}

void Game::Run() {
float vertices[] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};
  std::vector<Vertex<float>> vv;
  Vertex<float> v;
  std::vector<unsigned int> iv;
  for (int i = 0; i != sizeof(vertices)/4/5; ++i) {
    v.position = Vector3D<float>(vertices[5*i] ,vertices[5*i+1],vertices[5*i+2]);
    v.texture_coordinate = Vector2D<float>(vertices[5*i+3],vertices[5*i+4]);
    vv.push_back(v);
    iv.push_back(i);
  }
  Image2D::MakeFromFile(0, "1.png");
  std::vector<int> iiv {0};
  Mesh<float> m = Mesh<float>::Create(vv,iv,iiv);

  Shader::FetchById(0).Use();
  Shader::FetchById(0).Set1Int("sampler", 0);
  SetDrawLine(false);

  int begin_time = 0, end_time = 0, delta_time = 0, sleep_time = 0;
  float constexpr target_fps = 30;
  while (!glfwWindowShouldClose(window_ptr_->GetGLFWWindowPtr())) {
    glClearColor(window_clear_color_.x, window_clear_color_.y,
                 window_clear_color_.z, window_clear_color_.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ProcessInput();

    // all the units of time is millisecond,
    // and the unit of return value of glfwGetTime() is second
    end_time = glfwGetTime() * 1000;
    delta_time = end_time - begin_time;
    sleep_time = 1000 / target_fps - delta_time;
    printf("[Debug::Game::Run] delta_time_: %ims.\n", delta_time);
    printf("[Debug::Game::Run] sleep_time: %ims.\n", sleep_time);
    small_utility::utility_stuff::Sleep(sleep_time);

    begin_time = glfwGetTime() * 1000;

    Update(delta_time);
    Render();

    Shader::FetchById(0).Use();
    m.Render();

    glfwSwapBuffers(window_ptr_->GetGLFWWindowPtr());
    glfwPollEvents();
  }
}

void Game::ProcessInput() {
  if (keys_[GLFW_KEY_ESCAPE]) {
    glfwSetWindowShouldClose(window_ptr_->GetGLFWWindowPtr(), true);
  }
}

void Game::Update(float const delta_time) {
  for (auto &iterator : rendered_object_ptrs_) {
    iterator->Update(delta_time);
  }
}

void Game::Render() const {
  if (game_state_ == GameState::kGameStateOnMenu) {
  } else if (game_state_ == GameState::kGameStateStarted) {
    // sends data to gpu
    float r__ = 3.14f * sin(glfwGetTime());
    Matrix4x4<float> model = Rotate(r__,Vector3D<float>(1.3f,1,0));
    // TODO(small_sheep_ 1178550325@qq.com)
    // This "Translate" has the same error, probably because opengl storages
    // matrix in column-major instead of row-major -- our storage method.
    // Therefore, I would like to change my Matrix types to column-major Matrix,
    // but only to change the way of storaging.
    Matrix4x4<float> view = Translate(Vector3D<float>(0,0,r__));
    // TODO(small_sheep_ 1178550325@qq.com) Perspective results in black screen.
    Matrix4x4<float> projection = 
        Perspective(
            3.14f/4.0f, window_ptr_->GetWindowWidth() /
            static_cast<float>(window_ptr_->GetWindowHeight()), 0.1f, 100.0f);
    Matrix4x4<float> mvp_matrix = /*projection * view **/ model;
    UniformBlock::FetchById(0).FillBuffer(0, mvp_matrix);

    Shader::FetchById(0).Use();

    glEnable(GL_DEPTH_TEST);
    if (glIsEnabled(GL_BLEND)) {
      glDisable(GL_BLEND);
    }
    glEnable(GL_MULTISAMPLE);
    if (DrawLine()) {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    // for each -> render.
    for (auto &iterator : rendered_object_ptrs_) {
      //iterator->Render();
    }
  }
}

void Game::Delete() {
  window_ptr_.reset();
}
