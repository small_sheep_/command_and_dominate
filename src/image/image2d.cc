#include "command_and_dominate/image/image2d.h"
#include <exception>
#include "glad/glad.h"
#include "small_utility/log/logger.h"
#include "small_utility/string/string.h"
#include "stb_image/stb_image.h"

void Image2D::Initialize() {
  stbi_set_flip_vertically_on_load(true);
}

void Image2D::MakeFromFile(int const id, char const *const file_name) {
  int width, height, num_channels;
  unsigned char *const data = stbi_load(file_name, &width, &height,
                                        &num_channels, 0);
  if (!data) {
    using small_utility::string_stuff::String;
    String error_buffer("Can not make image from file ");
    error_buffer += file_name;
    small_utility::log_stuff::Warn(error_buffer.ConstData());
    throw std::runtime_error(error_buffer.ConstData());
  }
  Image2D::All().try_emplace(id, width, height, num_channels, data);
  if (Image2D::All().contains(id)) {
    Image2D::All().at(id).Generate();
  }
}

Image2D &Image2D::FetchById(int const id) {
  if (!all_.contains(id)) {
    throw std::runtime_error("Can not find image2d.");
  }
  return all_.at(id);
}

Image2D::Image2D(int const width, int const height, int const num_channels,
                 unsigned char *const data)
  // data must be the return value of function stbi_load.
    : width_(width), height_(height), num_channels_(num_channels), data_(data) {
  glGenTextures(1, &id_);
}

Image2D::~Image2D() {
  stbi_image_free(data_);
}

void Image2D::Bind() const {
  glBindTexture(GL_TEXTURE_2D, id_);
}

void Image2D::Generate() const {
  GLint internal_format = num_channels_ == 3 ? GL_RGB : GL_RGBA;

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, id_);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_NEAREST_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_NEAREST);
  // The sixth parameter in glTexImage2D must be 0, representing nothing.
  glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width_, height_, 0,
               internal_format, GL_UNSIGNED_BYTE, data_);
  glGenerateMipmap(GL_TEXTURE_2D);
}

std::map<int, Image2D> Image2D::all_;
