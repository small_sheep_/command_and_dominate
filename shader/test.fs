#version 330 core

in GeometryShaderOutput {
  vec3 fragment_position;
  vec3 normal;
  vec2 texture_coordinate;
} fs_in;

uniform sampler2D sampler;

out vec4 FragColor;

void main() {
  vec4 FragColor_buffer = vec4(1.0f);

  FragColor_buffer.xyz = fs_in.fragment_position;
  FragColor_buffer = FragColor_buffer * texture(sampler, fs_in.texture_coordinate);


  float gamma = 2.2f;
  FragColor_buffer.xyz = pow(FragColor_buffer.xyz, vec3(1.0f / gamma));
  FragColor = FragColor_buffer;
}
