#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coordinate;

layout (std140) uniform matrices {
  uniform mat4 mvp_matrix;
};

uniform vec3 model_position;
uniform mat4 model_rotation; // TODO : Please place this with Matrices::model

out VertexShaderOutput {
  vec3 fragment_position;
  vec3 normal;
  vec2 texture_coordinate;
} vs_out;

void main() {
  vec4 position_buffer = vec4(position, 1.0f);
  position_buffer = mvp_matrix * position_buffer;
  //position_buffer = vec4(position, 1.0f);

  vs_out.fragment_position = position_buffer.xyz;
  vs_out.texture_coordinate = texture_coordinate;

  gl_Position = position_buffer;
}
