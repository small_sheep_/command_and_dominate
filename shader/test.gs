#version 330 core
#define MAX_VERTICES 3

layout (triangles) in;

layout (triangle_strip, max_vertices = MAX_VERTICES) out;

in VertexShaderOutput {
  vec3 fragment_position;
  vec3 normal;
  vec2 texture_coordinate;
} gs_in[];

out GeometryShaderOutput {
  vec3 fragment_position;
  vec3 normal;
  vec2 texture_coordinate;
} gs_out;

void main() {
  for (int i = 0; i != MAX_VERTICES; ++i) {
    gl_Position               = gl_in[i].gl_Position;
    gs_out.fragment_position  = gs_in[i].fragment_position;
    gs_out.normal             = gs_in[i].normal;
    gs_out.texture_coordinate = gs_in[i].texture_coordinate;
    EmitVertex();
  }
  EndPrimitive();
}
